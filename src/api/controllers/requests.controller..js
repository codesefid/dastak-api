const responseService = require('../services/response.service');
const httpStatus = require('http-status');

const requestsModel = require('../models/requests.model');

module.exports = {
	async index(req, res, next) {
		try {
			const requests = await requestsModel.find();
			res.send(responseService.getResult(requests));
		} catch (error) {
			res.status(httpStatus.BAD_REQUEST).send(error);
		}
	},

	async show(req, res, next) {
		try {
			const requests = await requestsModel.findOne({ _id: req.params.id });
			res.send(responseService.getResult(requests));
		} catch (error) {
			res.status(httpStatus.BAD_REQUEST).send(error);
		}
	},

	async update(req, res, next) {
		const filter = { _id: req.params.id };

		try {
			let requests = await requestsModel.findOneAndUpdate(filter, req.body);
			if (requests) requests = await requestsModel.findOne(filter);
			res.send(responseService.getResult(requests));
		} catch (error) {
			res.status(httpStatus.BAD_REQUEST).send(error);
		}
	},

	async delete(req, res, next) {
		const filter = { _id: req.params.id };

		try {
			let requests = await requestsModel.findOneAndDelete(filter);
			res.send(responseService.getResult(requests));
		} catch (error) {
			res.status(httpStatus.BAD_REQUEST).send(error);
		}
	},

	async store(req, res, next) {
		try {
			const requests = await requestsModel.create(req.body);

			res.send(responseService.getResult(requests));
		} catch (error) {
			res.status(httpStatus.BAD_REQUEST).send(error);
		}
	},
};
