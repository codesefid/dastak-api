const responseService = require('../services/response.service');
const fileModel = require('../models/file.model')

module.exports = {
	async upload(req, res) {
		let sampleFile;
		let uploadPath;

		if (!req.files || Object.keys(req.files).length === 0) {
			return res.status(400).send('No files were uploaded.');
		}

		// The name of the input field (i.e. "sampleFile") is used to retrieve the uploaded file
		sampleFile = req.files.image;
		fileName = 'dastak-' + Math.floor(Math.random() * (9999 - 1000)) + 1000 + '-' + sampleFile.name;
		uploadPath = 'resources/image/' + fileName;

		// Use the mv() method to place the file somewhere on your server
		sampleFile.mv(uploadPath, function (err) {
			if (err) return res.status(500).send(err);
		});


		// BODY (the contents of the uploaded file - leave blank/remove to retain contents of original file.)
		const file = __basedir + "/" + uploadPath; //FILE_NAME (the name of the file to upload (if you don't specify KEY))


		try {
			// console.log('Success', data);
			let result = await fileModel.create({ url: uploadPath })
			return res.send(responseService.getResult(result, 'تصویر با موفقیت آپلود شد:)'));
		} catch (err) {
			console.log('Error', err);
			return res.send(responseService.getResult(err, 'خطایی پیدا شد! با پشتیبانی تماس بگیرید.'));
		}
	},

	async index(req, res, next) {
		try {
			const file = await fileModel.find();
			res.send(responseService.getResult(file));
		} catch (error) {
			res.status(httpStatus.BAD_REQUEST).send(error);
		}
	},
	
	async show(req, res, next) {
		try {
			const file = await fileModel.findOne({ _id: req.params.id });
			res.send(responseService.getResult(file));
		} catch (error) {
			res.status(httpStatus.BAD_REQUEST).send(error);
		}
	},
	
	async delete(req, res, next) {
		const filter = { _id: req.params.id };

		try {
			let file = await fileModel.findOneAndDelete(filter);
			res.send(responseService.getResult(file));
		} catch (error) {
			res.status(httpStatus.BAD_REQUEST).send(error);
		}
	}
};

