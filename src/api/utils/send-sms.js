const axios = require('axios');

const send = async (mobile, text) => {
	// send code massage
	var data = JSON.stringify({
		from: '50004001882813',
		to: mobile,
		text: text,
	});

	var config = {
		method: 'post',
		url: 'https://console.melipayamak.com/api/send/simple/3cb7d173f4444f409640e09294adacc3',
		headers: {
			'Content-Type': 'application/json',
		},
		data: data,
	};

	axios(config)
		.then(function(response) {
			// console.log(response.data);
			console.log('Send');
			return true;
		})
		.catch(function(error) {
			// console.log(error);
			// console.log("un")
			return false;
		});
};

module.exports = send;
