const express = require('express');
const router = express.Router();
const controller = require('../../controllers/file.controller');

router.post('/', controller.upload);
router.get('/', controller.index);
router.get('/:id', controller.show);
router.delete('/:id', controller.delete);
// router.get("/files", controller.getListFiles);
// router.get("/files/:name", controller.download);

module.exports = router;
