const express = require('express');
// import all the routes here
const requests = require('./requests.route');
const file = require('./file.route');
const validator = require('../../validations/user.validation')

const router = express.Router();

/**
 * GET v1/status
 */
router.get('/status', (req, res) => {
	res.json({
		message: 'OK',
		timestamp: new Date().toISOString(),
		IP: req.ip,
		URL: req.originalUrl,
	});
});

router.use('/requests', requests);
router.use('/file', file);

module.exports = router;
