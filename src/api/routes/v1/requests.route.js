const express = require('express');
const validate = require('express-validation');

const controller = require('../../controllers/requests.controller.');
// const validation = require('../../validations/user.validation');

const router = express.Router();

// un protected route
// Notice the same names of functions/object in validation and controller
router.route('/').get(controller.index);
router.route('/:id').get(controller.show);
router.route('/:id').put(controller.update);
router.route('/:id').delete(controller.delete);
router.route('/').post(controller.store);

// protected route
// router.route('/greet-me-protected').get(authenticated, validate(validation.me), controller.me);

module.exports = router;
