const mongoose = require('mongoose');

// Define schema
const Schema = mongoose.Schema;

const FileModelSchema = new Schema({
	url: { type: String, required: true }
});

// Compile model from schema
const files = mongoose.model('files', FileModelSchema);

module.exports = files;
