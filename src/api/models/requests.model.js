const mongoose = require('mongoose');

// Define schema
const Schema = mongoose.Schema;

const requestsModelSchema = new Schema({
	name: { type: String, required: true },
	phone: { type: String },
	status: { type: Object },
});

// Compile model from schema
const requestsModel = mongoose.model('requests', requestsModelSchema);

module.exports = requestsModel;
