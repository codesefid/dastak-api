const axios = require('axios');

class RequestService {
	static sendOTP(mobile, code) {
		var data = JSON.stringify({
			Code: code,
			Number: mobile,
		});

		var config = {
			method: 'post',
			url: 'https://ws.zarincall.ir/apiv2/Message/SendOTP',
			headers: {
				'Content-Type': 'application/json',
				Authorization: 'basic apikey:4ecc6627-b25d-42e9-94bb-4b8b1fe842ad:f3ada4bb-e524-417c-bb20-dcffdbed05ea',
			},
			data: data,
		};

		axios(config)
			.then(function(response) {
				console.log(JSON.stringify(response.data));
			})
			.catch(function(error) {
				console.log(error);
			});

		//   return(result)
	}
}

module.exports = RequestService;
