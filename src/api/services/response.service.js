class ResponseService {
	static notAuthenticated() {
		return { message: 'NOT_AUTHENTICATED' };
	}

	static getResult(payload, msg) {
		if (payload) {
			msg = 'عملیات با موفقیت انجام شد';
			return { status: 200, message: msg, data: payload };
		}
		if (!msg) msg = 'داده ای یافت نشد';
		return { status: 204, message: msg, data: payload };
	}
}

module.exports = ResponseService;
